package com.example.calculator_app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "costam";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnVertical = (Button) findViewById(R.id.simple_calculator);
        Button btnHorizontal = (Button) findViewById(R.id.advance_calculator);
        Button btnAbout = (Button) findViewById(R.id.information);
        Button btnExit= (Button) findViewById(R.id.exit);


        View.OnClickListener handler = new View.OnClickListener(){
            public void onClick(View v){
                switch (v.getId()){
                    case R.id.simple_calculator:
                        Intent intent = new Intent(MainActivity.this, SimpleCalculator.class);
                        MainActivity.this.startActivity(intent);

                        break;
                    case R.id.advance_calculator:
                        startActivity(new Intent(MainActivity.this, AdvanceCalculator.class));
                        break;

                    case R.id.information:
                        Intent info = new Intent(MainActivity.this, About.class);
                        MainActivity.this.startActivity(info);
                        break;

                    case R.id.exit:
                        finish();
                        System.exit(0);
                        Log.d(TAG, "Exit btn");
                }
            }
        };
        btnHorizontal.setOnClickListener(handler);
        btnVertical.setOnClickListener(handler);
        btnExit.setOnClickListener(handler);
        btnAbout.setOnClickListener(handler);

    }
}
