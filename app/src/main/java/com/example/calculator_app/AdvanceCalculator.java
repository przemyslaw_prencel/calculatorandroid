package com.example.calculator_app;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.tan;

public class AdvanceCalculator extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "DEBUG";

    TextView textViewFormula;
    String first = "";
    String second = "";
    String operator = "";
    Boolean lastlyCalculated = false;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // Save the values you need into "outState"
        outState.putString("first", first );
        outState.putString("second", second);
        outState.putString("operator", operator);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.advance_calculator);
        textViewFormula = findViewById(R.id.formula);

        if (savedInstanceState != null) {
            first = savedInstanceState.getString("first");
            second = savedInstanceState.getString("second");
            operator = savedInstanceState.getString("operator");

            if(!first.equals("") && second.equals("")){
                textViewFormula.setText(outputValue(first));
            }else{

                textViewFormula.setText(outputValue(second));
            }
        }

        findViewById(R.id.btn_0).setOnClickListener(this);
        findViewById(R.id.btn_1).setOnClickListener(this);
        findViewById(R.id.btn_2).setOnClickListener(this);
        findViewById(R.id.btn_3).setOnClickListener(this);
        findViewById(R.id.btn_4).setOnClickListener(this);
        findViewById(R.id.btn_5).setOnClickListener(this);
        findViewById(R.id.btn_6).setOnClickListener(this);
        findViewById(R.id.btn_7).setOnClickListener(this);
        findViewById(R.id.btn_8).setOnClickListener(this);
        findViewById(R.id.btn_9).setOnClickListener(this);
        findViewById(R.id.btn_AC).setOnClickListener(this);
        findViewById(R.id.btn_C).setOnClickListener(this);
        findViewById(R.id.btn_plus).setOnClickListener(this);
        findViewById(R.id.btn_minus).setOnClickListener(this);
        findViewById(R.id.btn_decimal).setOnClickListener(this);
        findViewById(R.id.btn_divide).setOnClickListener(this);
        findViewById(R.id.btn_multiply).setOnClickListener(this);
        findViewById(R.id.btn_change).setOnClickListener(this);
        findViewById(R.id.btn_equals).setOnClickListener(this);

        findViewById(R.id.btn_sin).setOnClickListener(this);
        findViewById(R.id.btn_cos).setOnClickListener(this);
        findViewById(R.id.btn_tan).setOnClickListener(this);
        findViewById(R.id.btn_log).setOnClickListener(this);
        findViewById(R.id.btn_ln).setOnClickListener(this);
        findViewById(R.id.btn_X2).setOnClickListener(this);
        findViewById(R.id.btn_XY).setOnClickListener(this);
        findViewById(R.id.btn_sqrt).setOnClickListener(this);
        findViewById(R.id.btn_percent).setOnClickListener(this);


    }

    public void setValue(String value) {

        String currentVar = operator.equals("") ? first : second;

        if(value.equals(".") && ((first.equals("") || currentVar.contains(".") || currentVar.equals("-")) || (!first.equals("") && second.equals(""))))
            return;

        if(lastlyCalculated == true) { // if you calculate and direct type in
            currentVar = "";
            lastlyCalculated = false;
        }

        currentVar = currentVar.concat(value);
        textViewFormula.setText(currentVar);


        if (operator.equals("")) first = currentVar;
        else second = currentVar;
    }

    public void handleChangeSign(){
        String currentVar = operator.equals("") ? first : second;

        if (currentVar.contains("-"))
            currentVar = currentVar.substring(1);

        else if(!currentVar.contains("-"))
            currentVar = "-" + currentVar;

        else
            currentVar = "-";

        textViewFormula.setText(outputValue(currentVar));
        if (operator.equals("")) first = currentVar;
        else second = currentVar;
    }

    public void setOperator(String value) {
        if (!first.equals("")){
            operator = value;
        }
    }

    public void clearOut(){
        first = "";
        second = "";
        operator = "";
        textViewFormula.setText("");
    }

    public void clear() {
        if(operator.equals("")) first = "";
        else second = "";

        textViewFormula.setText("");
    }

    public String outputValue(String value){
        Log.d(TAG, value);
        if(value.equals("0.000000") || value.equals(""))
            return "0";

        while (value.charAt(value.length()-1) == '0' || value.charAt(value.length()-1) == '.'){
            value = value.substring(0, value.length() - 1);
        }
        return value;
    }

    @SuppressLint("DefaultLocale")
    public void result(){
        String result = "";
        if (second.equals(""))
            return;

//        first = first.replaceAll(".", ".");
//        second = second.replaceAll(".", ".");

        switch (operator){
            case "+":
                result = String.format("%.6f", ((Double.parseDouble(first)) + Double.parseDouble(second)));
                break;

            case "-":
                result = String.format("%.6f", ((Double.parseDouble(first)) - Double.parseDouble(second)));
                break;

            case "*":
                result = String.format("%.6f", ((Double.parseDouble(first) * Double.parseDouble(second))));
                break;

            case "/":
                if(Double.parseDouble(second) == 0){
                    Toast.makeText(this, "Nie dziel przez zero!", Toast.LENGTH_LONG).show();
                    clearOut();
                }else{
                    result = String.format("%.6f", ((Double.parseDouble(first)) / Double.parseDouble(second)));
                }
                break;

            case "XY":
                if(second.equals("")){
                    Toast.makeText(this, "Ta operacja wymaga dwoch wartosc", Toast.LENGTH_LONG).show();
                    return;
                }

                result = String.format("%.6f", Math.pow(Double.parseDouble(first), Double.parseDouble(second)));
                break;

            default:
                break;
        }

        if(!result.equals("")){
            result = result.replaceAll(",", ".");
            first = result;
            second = "";
            operator = "";
            textViewFormula.setText(outputValue(result));
            lastlyCalculated = true;
        }

    }

    @SuppressLint("DefaultLocale")
    public void directResult(String operator) {
        if(first.equals(""))
            return;

        String result = "";
        String calculatedValue = !second.equals("") ? second : first;

        switch (operator) {
            case "sqrt":
                if(calculatedValue.charAt(0) == '-'){
                    Toast.makeText(this, "Nie moge policzyc pierwiastu z ujemnej liczby!", Toast.LENGTH_LONG).show();
                    clearOut();
                }else{
                    result = String.format("%.6f", Math.sqrt((Double.parseDouble(calculatedValue))));
                }
                break;

            case "X2":
                result = String.format("%.6f", ((Double.parseDouble(calculatedValue) * Double.parseDouble(calculatedValue))));
                break;

            case "ln":
                if(calculatedValue.charAt(0) == '-' || (calculatedValue.charAt(0) == '0' && calculatedValue.charAt(1) != '.')){
                    Toast.makeText(this, "Nie moge policzbyc logarytmu naturalne z liczby mnijeszej od 0", Toast.LENGTH_LONG).show();
                    clearOut();
                }else {
                    result = String.format("%.6f", Math.log(Double.parseDouble(calculatedValue)));
                }
                break;

            case "log":
                if(calculatedValue.charAt(0) == '-' || (calculatedValue.charAt(0) == '0' && calculatedValue.charAt(1) != '.')){
                    Toast.makeText(this, "Nie moge policzbyc logarytmu dziesietnego z liczby mnijeszej od 0", Toast.LENGTH_LONG).show();
                    clearOut();
                }else {
                    result = String.format("%.6f", Math.log10(Double.parseDouble(calculatedValue)));
                }
                break;

            case "percent":
                result = String.format("%.6f", (Double.parseDouble(calculatedValue) * 0.01));
                break;

            case "sin":
                result = String.format("%.6f", Math.sin(Double.parseDouble(calculatedValue)));
                break;

            case "cos":
                result = String.format("%.6f", Math.cos(Double.parseDouble(calculatedValue)));
                break;

            case "tan":
                result = String.format("%.6f", Math.tan(Double.parseDouble(calculatedValue)));
                break;

            default:
                break;
        }

        if(!result.equals("")){
            result = result.replaceAll(",", ".");
            textViewFormula.setText(outputValue(result));
            lastlyCalculated = true;
            if (!second.equals("")) second = result;
            else first = result;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_0:
                setValue(String.valueOf(0));
                break;

            case R.id.btn_1:
                setValue(String.valueOf(1));
                break;

            case R.id.btn_2:
                setValue(String.valueOf(2));
                break;

            case R.id.btn_3:
                setValue(String.valueOf(3));
                break;

            case R.id.btn_4:
                setValue(String.valueOf(4));
                break;

            case R.id.btn_5:
                setValue(String.valueOf(5));
                break;

            case R.id.btn_6:
                setValue(String.valueOf(6));
                break;

            case R.id.btn_7:
                setValue(String.valueOf(7));
                break;

            case R.id.btn_8:
                setValue(String.valueOf(8));
                break;

            case R.id.btn_9:
                setValue(String.valueOf(9));
                break;
            case R.id.btn_AC:
                clearOut();
                break;

            case R.id.btn_C:
                clear();
                break;

            case R.id.btn_decimal:
                setValue(String.valueOf("."));
                break;

            case R.id.btn_change:
                handleChangeSign();
                break;

            case R.id.btn_plus:
                setOperator("+");
                break;

            case R.id.btn_minus:
                setOperator("-");
                break;

            case R.id.btn_divide:
                setOperator("/");
                break;

            case R.id.btn_multiply:
                setOperator("*");
                break;

            case R.id.btn_sqrt:
                directResult("sqrt");
                break;

            case R.id.btn_X2:
                directResult("X2");
                break;

            case R.id.btn_ln:
                directResult("ln");
                break;

            case R.id.btn_log:
                directResult("log");
                break;

            case R.id.btn_percent:
                directResult("percent");
                break;

            case R.id.btn_sin:
                directResult("sin");
                break;

            case R.id.btn_cos:
                directResult("cos");
                break;

            case R.id.btn_tan:
                directResult("tan");
                break;

            case R.id.btn_equals:
                result();
                break;

            case R.id.btn_XY:
                setOperator("XY");
                break;

            default:
                break;
        }
//        if(operator.equals("XY") && !second.equals(""))
//            result();

        Log.d(TAG, "first: " + first + "\t operator: " + operator + "\t second: " + second);
    }
}