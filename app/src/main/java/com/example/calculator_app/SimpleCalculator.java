package com.example.calculator_app;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


public class SimpleCalculator extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "DEBUG";

    TextView textViewFormula;
    TextView textViewResult;
    String first = "";
    String second = "";
    String operator = "";
    Boolean lastlyCalculated = false;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // Save the values you need into "outState"
        outState.putString("first", first );
        outState.putString("second", second);
        outState.putString("operator", operator);
        super.onSaveInstanceState(outState);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.simple_calculator);
        textViewFormula = findViewById(R.id.formula);

        if (savedInstanceState != null) {
            first = savedInstanceState.getString("first");
            second = savedInstanceState.getString("second");
            operator = savedInstanceState.getString("operator");

            if(!first.equals("") && second.equals("")){
                textViewFormula.setText(outputValue(first));
            }else{
                textViewFormula.setText(outputValue(second));
            }
        }

        findViewById(R.id.btn_0).setOnClickListener(this);
        findViewById(R.id.btn_1).setOnClickListener(this);
        findViewById(R.id.btn_2).setOnClickListener(this);
        findViewById(R.id.btn_3).setOnClickListener(this);
        findViewById(R.id.btn_4).setOnClickListener(this);
        findViewById(R.id.btn_5).setOnClickListener(this);
        findViewById(R.id.btn_6).setOnClickListener(this);
        findViewById(R.id.btn_7).setOnClickListener(this);
        findViewById(R.id.btn_8).setOnClickListener(this);
        findViewById(R.id.btn_9).setOnClickListener(this);
        findViewById(R.id.btn_AC).setOnClickListener(this);
        findViewById(R.id.btn_C).setOnClickListener(this);
        findViewById(R.id.btn_plus).setOnClickListener(this);
        findViewById(R.id.btn_minus).setOnClickListener(this);
        findViewById(R.id.btn_decimal).setOnClickListener(this);
        findViewById(R.id.btn_divide).setOnClickListener(this);
        findViewById(R.id.btn_multiply).setOnClickListener(this);
        findViewById(R.id.btn_change).setOnClickListener(this);
        findViewById(R.id.btn_equals).setOnClickListener(this);
    }


    public void setValue(String value) {

        String currentVar = operator.equals("") ? first : second;

        if(value.equals(".") && ((first.equals("") || currentVar.contains(".") || currentVar.equals("-")) || (!first.equals("") && second.equals(""))))
            return;

        if(lastlyCalculated == true) { // if you calculate and direct type in
            currentVar = "";
            lastlyCalculated = false;
        }

        currentVar = currentVar.concat(value);
        textViewFormula.setText(currentVar);


        if (operator.equals("")) first = currentVar;
        else second = currentVar;
    }

    public void handleChangeSign(){
        String currentVar = operator.equals("") ? first : second;

        if (currentVar.contains("-"))
            currentVar = currentVar.substring(1);

        else if(!currentVar.contains("-"))
            currentVar = "-" + currentVar;

        else
            currentVar = "-";

        textViewFormula.setText(outputValue(currentVar));
        if (operator.equals("")) first = currentVar;
        else second = currentVar;
    }

    public void setOperator(String value) {
        if (!first.equals("")){
            operator = value;
        }
    }

    public void clearOut(){
        first = "";
        second = "";
        operator = "";
        textViewFormula.setText("");
    }

    public void clear() {
        if(operator.equals("")) first = "";
        else second = "";

        textViewFormula.setText("");
    }

    public String outputValue(String value){
        Log.d(TAG, value);
        if(value.equals("0.000000") || value.equals(""))
            return "0";

        while (value.charAt(value.length()-1) == '0' || value.charAt(value.length()-1) == '.'){
            value = value.substring(0, value.length() - 1);
        }
        Log.d(TAG, value);
        return value;
    }

    @SuppressLint("DefaultLocale")
    public void result(){
        String result = "";
        if (second.equals("")){
            return;
        }

        switch (operator){
            case "+":
                result = String.format("%.6f", ((Double.parseDouble(first)) + Double.parseDouble(second)));
                break;

            case "-":
                result = String.format("%.6f", ((Double.parseDouble(first)) - Double.parseDouble(second)));
                break;

            case "*":
                result = String.format("%.6f", ((Double.parseDouble(first) * Double.parseDouble(second))));
                break;

            case "/":
                if(Double.parseDouble(second) == 0){
                    Toast.makeText(this, "Nie dziel przez zero!", Toast.LENGTH_LONG).show();
                    clearOut();
                }else{
                    result = String.format("%.6f", ((Double.parseDouble(first)) / Double.parseDouble(second)));
                }
                break;

            default:
                break;
        }

        if(!result.equals("")){
            result = result.replaceAll(",", ".");
            first = result;
            second = "";
            operator = "";
            lastlyCalculated = true;
            textViewFormula.setText(outputValue(result));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_0:
                setValue(String.valueOf(0));
                break;

            case R.id.btn_1:
                setValue(String.valueOf(1));
                break;

            case R.id.btn_2:
                setValue(String.valueOf(2));
                break;

            case R.id.btn_3:
                setValue(String.valueOf(3));
                break;

            case R.id.btn_4:
                setValue(String.valueOf(4));
                break;

            case R.id.btn_5:
                setValue(String.valueOf(5));
                break;

            case R.id.btn_6:
                setValue(String.valueOf(6));
                break;

            case R.id.btn_7:
                setValue(String.valueOf(7));
                break;

            case R.id.btn_8:
                setValue(String.valueOf(8));
                break;

            case R.id.btn_9:
                setValue(String.valueOf(9));
                break;
            case R.id.btn_AC:
                clearOut();
                break;

            case R.id.btn_C:
                clear();
                break;

            case R.id.btn_decimal:
                setValue(String.valueOf("."));
                break;

            case R.id.btn_change:
                handleChangeSign();
                break;

            case R.id.btn_plus:
                setOperator("+");
                break;

            case R.id.btn_minus:
                setOperator("-");
                break;

            case R.id.btn_divide:
                setOperator("/");
                break;

            case R.id.btn_multiply:
                setOperator("*");
                break;

            case R.id.btn_equals:
                result();
                break;

            default:
                break;
        }
        Log.d(TAG, "first: " + first + "\t operator: " + operator + "\t second: " + second);
    }
}